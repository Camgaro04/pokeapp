import { Component } from '@angular/core';
import { PokemonesService } from 'src/app/services/pokemones.service';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  pokemonItems:Item[]=[];

  constructor(private pokemonService:PokemonesService) {
    this.pokemonService.getItemList().subscribe((data:any)=>{
       this.pokemonItems = data;
    });
  }
}
