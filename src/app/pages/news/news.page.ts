import { Component, OnInit } from '@angular/core';
import { NewsModel } from '../../models/new.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

  noticeElement:NewsModel;
  constructor() { }

  ngOnInit() {
    this.getNewInformation();
  }

  getNewInformation(){
     this.noticeElement = JSON.parse(localStorage.getItem('newInfo'));
  }

}
