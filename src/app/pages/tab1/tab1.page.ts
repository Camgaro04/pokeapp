import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { NewsModel } from '../../models/new.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  listNews:NewsModel[] =[];

  constructor(
    private apiService:ApiService,
    private router:Router
    ) {
    this.getNews();
  }


  navigateNew(element:any){
    localStorage.setItem('newInfo', JSON.stringify(element));
    this.router.navigateByUrl('/tabs/tab1/news')
  }

  getNews(){
    this.apiService.getNews().subscribe((data)=>{
       console.log(data);
      this.listNews = data;
    }, (error)=>{
      console.log(error);
    });
  }

}
