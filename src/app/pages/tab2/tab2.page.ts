import { Component } from '@angular/core';
import { PokemonesService } from 'src/app/services/pokemones.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Router, NavigationExtras } from '@angular/router';
import { TypesImage, PokemonInfo } from 'src/app/models/pokemoninfo.model';
import { element } from 'protractor';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})


export class Tab2Page {

  pokemones: Pokemon [] = [];
  typesImage:TypesImage[] =[];

  constructor(private pokemonesService:PokemonesService,
              private router:Router) {}

  
  navigateProfile(pokemonId:any){
    this.router.navigate(['/tabs/tab2/pokemon-profile',pokemonId]);
  }


  ngOnInit(): void {
    this.pokemonesService.getPokemonList().subscribe((data:any)=>{
        this.pokemones = data;
      },(error)=>{
        console.log(error);
    });
  }
}
