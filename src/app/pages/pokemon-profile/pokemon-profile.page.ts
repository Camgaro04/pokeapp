import { Component, OnInit } from '@angular/core';
import { NgModel, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PokemonesService } from 'src/app/services/pokemones.service';
import { PokemonInfo, TypesImage } from 'src/app/models/pokemoninfo.model';

@Component({
  selector: 'app-pokemon-profile',
  templateUrl: './pokemon-profile.page.html',
  styleUrls: ['./pokemon-profile.page.scss'],
})
export class PokemonProfilePage implements OnInit {
  pokemonInfo:PokemonInfo;
  url:string = '../../../assets/bug.png'

  constructor(private route: ActivatedRoute,
    private pokemonService:PokemonesService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
       const id = params['id'];
       this.pokemonService.getPokemonInfoById(id).subscribe((data:PokemonInfo)=>{
          let pokemontypes:TypesImage[]=[];
          this.pokemonInfo = data;
          this.pokemonInfo.types.forEach(type=>{
              pokemontypes.push({name:type.type.name, url:this.getTypeImage(type.type.name)});
          });
          this.pokemonInfo.images_types = pokemontypes;
       });
    });
  }

  getTypeImage(pokemonType:string){
    switch(pokemonType){
        case 'fire':
          return '../../../assets/TypesFire.png';
        case 'water':
          return '../../../assets/TypesWater.png'; 
        case 'normal':
          return '../../../assets/TypesNormal.png';
        case 'fighting':
          return '../../../assets/TypesFight.png';  
        case 'flying':
          return '../../../assets/TypesFlying.png'; 
        case 'poison':
          return '../../../assets/TypesPoison.png';
        case 'ground':
          return '../../../assets/TypesGround.png';
        case 'rock':
          return '../../../assets/TypesRock.png';
        case 'bug':
          return '../../../assets/TypesBug.png';
        case 'ghost':
          return '../../../assets/TypesGhost.png';
        case 'steel':
          return '../../../assets/TypesSteel.png';
        case 'grass':
          return '../../../assets/TypesGrass.png';
        case 'electric':
          return '../../../assets/TypesElectric.png';
        case 'psychic':
          return '../../../assets/TypesPsychic.png';
        case 'ice':
          return '../../../assets/TypesIce.png';
        case 'dragon':
          return '../../../assets/TypesDragon.png';
        case 'dark':
          return '../../../assets/TypesDark.png';
        case 'fairy':
          return '../../../assets/TypesFairy.png';
        case 'shadow':
          return '../../../assets/TypesDark.png';
        case 'unknown':
          return '../../../assets/TypesNormal.png';
        default:
          return '../../../assets/TypesFairy.png';
    }
  }
}
