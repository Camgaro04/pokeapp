import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PokemonProfilePage } from './pokemon-profile.page';

describe('PokemonProfilePage', () => {
  let component: PokemonProfilePage;
  let fixture: ComponentFixture<PokemonProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
