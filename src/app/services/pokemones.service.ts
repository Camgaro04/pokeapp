import { Injectable } from '@angular/core';
import {Pokemon} from '../models/pokemon.model'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";
import { Item } from '../models/item.model';
import { PokemonInfo, TypesImage } from '../models/pokemoninfo.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonesService {

  pokemonsList: Pokemon [] = [];
  itemsPokemonList : Item [] = [];

  constructor(private httpClient:HttpClient) {}


  getPokemonList(){
    const url = 'https://pokeapi.co/api/v2/pokemon/';
    return this.httpClient.get(url).pipe(map((data:any)=>{
      data.results.forEach((element)=>{
          let newPokemon = new Pokemon();
          newPokemon.name = element.name;
          this.getPokemonInfo(element.url).subscribe((data:PokemonInfo)=>{
              let pokemonTypes:string[] = [];
              let imagesTypes:TypesImage[] = [];
              newPokemon.id = data.game_indices[0].game_index;
              newPokemon.info = data;
              newPokemon.url_image = data.sprites.front_default;
              data.types.forEach(types=>{
                imagesTypes.push({name:types.type.name,url:this.getTypeImage(types.type.name)});
                pokemonTypes.push(types.type.name);
              });
              newPokemon.type = pokemonTypes;
              newPokemon.image_types = imagesTypes;
          });
          
          this.pokemonsList.push(newPokemon);
      });
      return this.pokemonsList;
    }));
  }

  getPokemonInfo(url:string){
    return this.httpClient.get(url).pipe(map((data)=>{
      return data;
    }));
  }

  getPokemonInfoById(id:string){
    const url = 'https://pokeapi.co/api/v2/pokemon/'+id+'/';
    return this.httpClient.get(url).pipe(map((data)=>{
      return data;
    }));
  }


  getItemList(){
    const url = 'https://pokeapi.co/api/v2/item/';
    return this.httpClient.get(url).pipe(map((data:any)=>{
       data.results.forEach(element=>{
          let itemList = new Item();
          itemList.name = element.name;
          this.getItemUrlInfo(element.url).subscribe((data:any)=>{
            itemList.info = JSON.stringify(data);
            itemList.url_Image = data.sprites.default;
          }); 
          this.itemsPokemonList.push(itemList);
       });
       return this.itemsPokemonList;
    }));  
  }

  getItemUrlInfo(url){
    return this.httpClient.get(url).pipe(map((data)=>{
      return data;
    }));
  }


  getTypeImage(pokemonType:string){
    switch(pokemonType){
        case 'fire':
          return '../../../assets/TypesFire.png';
        case 'water':
          return '../../../assets/TypesWater.png'; 
        case 'normal':
          return '../../../assets/TypesNormal.png';
        case 'fighting':
          return '../../../assets/TypesFight.png';  
        case 'flying':
          return '../../../assets/TypesFlying.png'; 
        case 'poison':
          return '../../../assets/TypesPoison.png';
        case 'ground':
          return '../../../assets/TypesGround.png';
        case 'rock':
          return '../../../assets/TypesRock.png';
        case 'bug':
          return '../../../assets/TypesBug.png';
        case 'ghost':
          return '../../../assets/TypesGhost.png';
        case 'steel':
          return '../../../assets/TypesSteel.png';
        case 'grass':
          return '../../../assets/TypesGrass.png';
        case 'electric':
          return '../../../assets/TypesElectric.png';
        case 'psychic':
          return '../../../assets/TypesPsychic.png';
        case 'ice':
          return '../../../assets/TypesIce.png';
        case 'dragon':
          return '../../../assets/TypesDragon.png';
        case 'dark':
          return '../../../assets/TypesDark.png';
        case 'fairy':
          return '../../../assets/TypesFairy.png';
        case 'shadow':
          return '../../../assets/TypesDark.png';
        case 'unknown':
          return '../../../assets/TypesNormal.png';
        default:
          return '../../../assets/TypesFairy.png';
    }
  }

}
