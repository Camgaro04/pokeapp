import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NewsModel } from '../models/new.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  listNews:NewsModel[] =[];

  constructor(private httpClient:HttpClient) { }

  getNews() {
   let url = 'https://api.pokemon.com/es/api/news/?index=0&count=10';
    return this.httpClient.get(url).pipe(map((data:any)=>{
      data.forEach(element => {
        let notice = new NewsModel();
        
        notice.title = element.title;
        notice.date = element.date;
        notice.shortDescription = element.shortDescription;
        notice.image = element.image;
        this.listNews.push(notice);
      });
      return this.listNews;
    }));
  }

}
