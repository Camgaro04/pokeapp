
export class NewsModel{
    title: string;
    date: Date;
    shortDescription: string;
    image: string;

}