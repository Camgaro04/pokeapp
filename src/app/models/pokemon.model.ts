import { PokemonInfo, TypesImage } from './pokemoninfo.model';

export class Pokemon{
    id: number;
    name: string;
    info: PokemonInfo;
    type:string[];
    url_image: string;
    image_types:TypesImage[];
}